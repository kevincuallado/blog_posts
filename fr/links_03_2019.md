Title: Les liens intéressants du mois de Mars 2019.
Date: 2019-04-13
Category: review

## Articles
### [Chasing 10X: Leveraging A Poor Memory In Engineering](https://senrigan.io/blog/chasing-10x-leveraging-a-poor-memory-in-software-engineering)
Articles présentant la méthode de répétition espacée pour la mémorisation, l'apprentissage ainsi que son application en tant qu'ingénieurs

### [Linux Desktop Setup](https://hookrace.net/blog/linux-desktop-setup/)
Dennis Felsing nous présente un échantillon de son environnement de travail sous GNU/Linux.

### [How I'm still not using GUIs in 2019: A guide to the terminal](https://www.lucasfcosta.com/2019/02/10/terminal-guide-2019.html)
Lucas F. Costa nous présente son environnement de développement utilisant uniquement des outils dans le terminal.

### [Unix as IDE](https://sanctum.geek.nz/arabesque/series/unix-as-ide/)
Suite d'articles sur les usages et la mise en place d'un système UNIX en tant qu'environnement de développement intégré.

### [Fancy Vim Plugins](https://danyspin97.org/blog/fancy-vim-plugins/)
Danilo Spinella nous présente plusieurs plugins pour Vim.

### [How I'm able to take notes in mathematics lectures using LaTeX and Vim](https://castel.dev/post/lecture-notes-1/)
Excellent article présentant un environnement de travail utilisant LaTeX et Vim pour la prise de notes mathématiques.

### [Gemini: A Modern LaTeX Poster Theme](https://www.anishathalye.com/2018/07/19/gemini-a-modern-beamerposter-theme/)
Cet article présente un thème pour faire des affiches avec LaTeX.

### [Makefiles, Best Practices](https://danyspin97.org/blog/makefiles-best-practices/)
Danilo Spinella nous présente les bonnes pratiques de rédaction d'un fichier MakeFiles.

### [What is Commento? | The Open Source Disqus Alternative](https://www.256kilobytes.com/content/show/4957/what-is-commento-the-open-source-disqus-alternative)
Cet article nous parle de Commento, une alternative libre, rapide et orienté vie privée à Disqus, Facebook comments et autres.

### [How to take back control of /etc/resolv.conf on Linux](https://www.ctrl.blog/entry/resolvconf-tutorial)
Articles expliquant comment reprendre le contrôle du fichier /etc/resolv.conf utilisé pour la resolution DNS.

### [A surprisingly arcane little Unix shell pipeline example](https://utcc.utoronto.ca/~cks/space/blog/unix/ShellPipelineIndeterminate)
L'article montre que le résultat de commandes utilisant echo et le caractère pipe '|' peut être surprenant. 

### [XKCD-style plots in Matplotlib](https://jakevdp.github.io/blog/2012/10/07/xkcd-style-plots-in-matplotlib/)
L'article présente une méthode pour afficher des graphiques dans le même style que le web comics [XKCD](https://xkcd.com/).

### [Vertically Scaling PostgreSQL](http://pgdash.io/blog/scaling-postgres.html)
pgDash nous présente des options de configurations pour optimiser PostgreSQL.

### [Common misconceptions about IPv6 security](https://blog.apnic.net/2019/03/18/common-misconceptions-about-ipv6-security/)
L'article mais au clair les idées fausses à propos de la sécurité avec le protocole IPv6.

### [Defining a Distinguished Engineer](https://blog.jessfraz.com/post/defining-a-distinguished-engineer/)
Jessie Frazzel décrit ce qui pour elle devrait être un bon ingénieur.

### [FreeBSD sur un ThinkPad](https://adminblog.foucry.net//2019/03/05/FreeBSD-ThinkPad/)
Jacques Foucry décrit l'installation de FreeBSD sur un ThinkPad x280

### [Networking tool comics!](https://jvns.ca/blog/2019/02/10/a-few-networking-tool-comics/)
Julia Evans nous présente les outils réseaux au travers d'un [Zine](https://en.wikipedia.org/wiki/Zine).

### [New zine: Oh shit, git!](https://jvns.ca/blog/2018/10/27/new-zine--oh-shit--git-/)
Julia Evans explique comment, sous git, il est possible de revenir en arrière en cas d'erreur.

### [k3d - A fast kubernetes dev environment](https://blog.zeerorg.site/post/k3d-kubernetes-dev-env)
Rishabh nous présente un environnement de dev Kubernetes sur sa machine locale.

### [Why we moved our servers to Iceland](https://blog.simpleanalytics.io/why-we-moved-our-servers-to-iceland)
Article vie privée et respect de ses utilisateurs ; on découvre les raisons qui on pousse l'entreprise Simple Ananalytics a migré ses serveurs en Islande.

### [Easy to Remember Color Guide for Non-Designers](https://sendwithses.gitbook.io/helpdocs/random-stuff/easy-to-remember-color-guide-for-non-designers)
Astuce pour bien choisir les couleurs dans les UI.

### [How to Deliver Constructive Feedback in Difficult Situations](https://medium.dave-bailey.com/the-essential-guide-to-difficult-conversations-41f736e63ccf)
Dave Bailey écrit sur le sujet de la communication non-violente et son usage dans les situations difficiles. 

### [Making grass flow like water to save planet](https://www.charmindustrial.com/blog/2019/3/17/making-grass-flow-like-water)
Articles décrivant les problèmes rencontrés lors du développement d'outils de production d'hydrogène.

## Outils
### [The Bash Guide](https://guide.bash.academy/)
Guide complet de Bash.

### [Awesome Bash](https://github.com/awesome-lists/awesome-bash)
Liste de ressource sur Bash.

### [Cipherli.st](https://cipherli.st/)
Site avec des conseils de configurations sécurisées de logiciels

### [Cookin' with Rust](https://rust-lang-nursery.github.io/rust-cookbook/)
Suite d'exemple de bonne pratique d'usage du language de programmation Rust.

### [Learn Vimscript the Hard Way](http://learnvimscriptthehardway.stevelosh.com/)
Ressources pour apprendre Vimscript, le language utilise pour faire des plugins sous Vim.

### [The TAO of TMUX](https://leanpub.com/the-tao-of-tmux/read)
Truc et astuces pour TMUX
